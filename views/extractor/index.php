<?php
/**
 * @var \app\forms\RequestForm          $requestForm
 * @var \yii\data\DataProviderInterface $taxpayerDataProvider
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<div class="extractor-index">
    <?php
    $form = ActiveForm::begin([
        'id'     => 'search-form',
        'method' => 'post',
        'action' => \yii\helpers\Url::toRoute('extractor/search'),
    ]) ?>
    <?= $form->field($requestForm, 'iin') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $taxpayerDataProvider,
        'columns'      => [
            [
                'class' => \yii\grid\SerialColumn::class,
            ],
            'iin',
            'name',
            'total_arrear',
            'total_pension_contribution_arrear',
            'total_social_contribution_arrear',
//            'created_at:datetime',
//            'updated_at:datetime',
            [
                'class'    => \yii\grid\ActionColumn::class,
                'template' => '{view}',
            ],
        ],
    ]) ?>
</div>