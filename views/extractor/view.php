<?php
/**
 * @var \app\models\Taxpayer $taxpayer
 */
?>
<div class="extractor-view">
    <?= \yii\bootstrap\Html::a('List', ['index'], ['class' => 'btn btn-success']) ?>
    <hr>
    <?= \yii\widgets\DetailView::widget([
        'model' => $taxpayer,
    ]) ?>
    <hr>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'   => $taxpayer->getTaxInfos(),
        ]),
        'columns' => [
            'agencyCode',
            'arrearTypeCode',
            'bcc_tax_arrear',
            'bcc_poena_arrear',
            'bcc_fine_arrear',
            'bcc_total_arrear',
        ],
    ]) ?>
</div>
