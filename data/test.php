<?php

return [
    'name'                         => 'ВОХМЯКОВ ВАДИМ ПЕТРОВИЧ',
    'iinBin'                       => '851216350129',
    'totalArrear'                  => 123.56,
    'pensionContributionArrear'    => 0,
    'socialContributionArrear'     => 0,
    'appealledAmount'              => null,
    'modifiedTermsAmount'          => null,
    'rehabilitaionProcedureAmount' => null,
    'sendTime'                     => 1507884591000,
    'taxOrgInfo'                   =>
        [
            0 =>
                [
                    'nameRu'                       => 'Республиканское государственное учреждение "Управление государственных доходов по Костанайскому району Департамента государственных доходов по Костанайской области Комитета государственных доходов Министерства финансов Республики Казахстан"',
                    'nameKk'                       => '"Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Қостанай облысы бойынша Мемлекеттік кірістер департаментінің Қостанай ауданы бойынша Мемлекеттік кірістер басқармасы" республикалық мемлекеттік мекемесі',
                    'charCode'                     => '3908',
                    'reportAcrualDate'             => 1507831200000,
                    'totalArrear'                  => 0,
                    'totalTaxArrear'               => 0,
                    'pensionContributionArrear'    => 0,
                    'socialContributionArrear'     => 0,
                    'appealledAmount'              => null,
                    'modifiedTermsAmount'          => null,
                    'rehabilitaionProcedureAmount' => null,
                    'taxPayerInfo'                 =>
                        [
                            0 =>
                                [
                                    'nameRu'         => 'ВОХМЯКОВ ВАДИМ ПЕТРОВИЧ',
                                    'iinBin'         => '851216350129',
                                    'totalArrear'    => 0,
                                    'bccArrearsInfo' =>
                                        [
                                            0 =>
                                                [
                                                    'bcc'         => '104102',
                                                    'bccNameRu'   => 'Hалог на имущество физических лиц',
                                                    'bccNameKz'   => 'Жеке тұлғалардың мүлкiне салынатын салық',
                                                    'taxArrear'   => 0,
                                                    'poenaArrear' => 0,
                                                    'fineArrear'  => 0,
                                                    'totalArrear' => 0,
                                                ],
                                            1 =>
                                                [
                                                    'bcc'         => '104302',
                                                    'bccNameRu'   => 'Земельный налог с физических лиц на земли населенных пунктов',
                                                    'bccNameKz'   => 'Елдi мекендер жерлерiне жеке тұлғалардан алынатын жер салығы',
                                                    'taxArrear'   => 0,
                                                    'poenaArrear' => 0,
                                                    'fineArrear'  => 0,
                                                    'totalArrear' => 0,
                                                ],
                                        ],
                                ],
                        ],
                ],
            1 =>
                [
                    'nameRu'                       => 'Республиканское государственное учреждение "Управление государственных доходов по городу Костанай Департамента государственных доходов по Костанайской области Комитета государственных доходов Министерства финансов Республики Казахстан"',
                    'nameKk'                       => '"Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Қостанай облысы бойынша Мемлекеттік кірістер департаментінің Қостанай қаласы бойынша Мемлекеттік кірістер басқармасы" республикалық мемлекеттік мекемесі',
                    'charCode'                     => '3917',
                    'reportAcrualDate'             => 1507831200000,
                    'totalArrear'                  => 0,
                    'totalTaxArrear'               => 0,
                    'pensionContributionArrear'    => 0,
                    'socialContributionArrear'     => 0,
                    'appealledAmount'              => null,
                    'modifiedTermsAmount'          => null,
                    'rehabilitaionProcedureAmount' => null,
                    'taxPayerInfo'                 =>
                        [
                            0 =>
                                [
                                    'nameRu'         => 'ВОХМЯКОВ ВАДИМ ПЕТРОВИЧ',
                                    'iinBin'         => '851216350129',
                                    'totalArrear'    => 0,
                                    'bccArrearsInfo' =>
                                        [
                                            0 =>
                                                [
                                                    'bcc'         => '104402',
                                                    'bccNameRu'   => 'Hалог на транспортные средства с физических лиц',
                                                    'bccNameKz'   => 'Жеке тұлғалардың көлiк құралдарына салынатын салық',
                                                    'taxArrear'   => 0,
                                                    'poenaArrear' => 0,
                                                    'fineArrear'  => 0,
                                                    'totalArrear' => 0,
                                                ],
                                        ],
                                ],
                        ],
                ],
        ],
];