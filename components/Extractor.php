<?php
/**
 *
 */

namespace app\components;

use app\services\AntiCaptchaException;
use app\services\AntiCaptchaService;
use app\services\DumperException;
use app\services\DumperService;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class ExtractorComponent
 *
 * @package app\components
 */
class Extractor extends BaseObject
{
    /**
     * @param string|null $iin
     *
     * @return array
     */
    public function pull(string $iin = null): array
    {
        $service = \Yii::createObject([
            'class'       => AntiCaptchaService::class,
            'publicKey'   => ArrayHelper::getValue(\Yii::$app->params, 'antiCaptcha.publicKey'),
            'captchasDir' => ArrayHelper::getValue(\Yii::$app->params, 'antiCaptcha.dir'),
        ]);

        try {
            return $service->pull($iin);
        } catch (AntiCaptchaException $exception) {
            \Yii::error($exception->getMessage(), 'extractor.pull');

            return [];
        }
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function dump(array $data): bool
    {
        $service = \Yii::createObject([
            'class' => DumperService::class,
        ]);

        try {
            $service->dump($data);

            return true;
        } catch (DumperException $exception) {
            \Yii::error($exception->getMessage(), 'extractor.dump');

            return false;
        }
    }
}