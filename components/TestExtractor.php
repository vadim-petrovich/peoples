<?php
/**
 *
 */

namespace app\components;

/**
 * Class TestExtractor
 *
 * @package app\components
 */
class TestExtractor extends Extractor
{
    /**
     * @inheritdoc
     */
    public function pull(string $iin = null): array
    {
        return require \Yii::getAlias('@app/data/test.php');
    }
}