<?php

use yii\db\Migration;

/**
 * Class m171013_025853_init
 */
class m171013_025853_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%taxpayer}}', [
            'iin'                               => $this->string(12)->notNull()->comment('ИИН'),
            'name'                              => $this->string()->notNull()->comment('Наименование налогоплательщика'),
            'total_arrear'                      => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Всего задолженности'),
            'total_pension_contribution_arrear' => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Задолженность по обязательным пенсионным взносам'),
            'total_social_contribution_arrear'  => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Задолженность по социальным отчислениям'),
            'created_at'                        => $this->dateTime()->notNull()->comment('Дата создания'),
            'updated_at'                        => $this->dateTime()->comment('Дата обновления'),
        ]);
        $this->addPrimaryKey('pk_taxpayer_iin', '{{%taxpayer}}', 'iin');

        /**
         *
         */
        $this->createTable('{{%agency}}', [
            'code'   => $this->integer()->notNull()->comment('Код учреждения'),
            'nameRu' => $this->text()->notNull()->comment('Наименование учреждения на русском'),
            'nameKz' => $this->text()->notNull()->comment('Наименование учреждения на казахском'),
        ]);
        $this->addPrimaryKey('pk_agency_code', '{{%agency}}', 'code');

        /**
         *
         */
        $this->createTable('{{%arrear_type}}', [
            'code'   => $this->integer()->notNull()->comment('Код КБК'),
            'nameRu' => $this->text()->notNull()->comment('Наименование КБК на русском'),
            'nameKz' => $this->text()->notNull()->comment('Наименование КБК на русском'),
        ]);
        $this->addPrimaryKey('pk_arrear_type_code', '{{%arrear_type}}', 'code');

        /**
         *
         */
        $this->createTable('{{%tax_info}}', [
            'taxpayer_iin'     => $this->string(12)->notNull()->comment('Налогоплательщик'),
            'agency_code'      => $this->integer()->notNull()->comment('Наименование учреждения'),
            'arrear_type_code' => $this->integer()->notNull()->comment('Тип налоговой задолженности'),

            'bcc_tax_arrear'   => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Задолженность по сумме налога'),
            'bcc_poena_arrear' => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Задолженность по сумме пени'),
            'bcc_fine_arrear'  => $this->decimal(10,
                2)->notNull()->defaultValue(0)->comment('Задолженность по сумме штрафа'),
            'bcc_total_arrear' => $this->decimal(10, 2)->notNull()->defaultValue(0)->comment('Всего задолженности'),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
        ]);
        $this->addPrimaryKey('pk_tax_info', '{{%tax_info}}', ['taxpayer_iin', 'agency_code', 'arrear_type_code']);

        $this->createIndex('u_idx_tax_info', '{{%tax_info}}', ['taxpayer_iin', 'agency_code', 'arrear_type_code'],
            true);
        $this->addForeignKey('fk_tax_info_taxpayer', '{{%tax_info}}', 'taxpayer_iin', '{{%taxpayer}}', 'iin', 'CASCADE',
            'CASCADE');
        $this->addForeignKey('fk_tax_info_agency', '{{%tax_info}}', 'agency_code', '{{%agency}}', 'code', 'CASCADE',
            'CASCADE');
        $this->addForeignKey('fk_tax_info_arrear_type', '{{%tax_info}}', 'arrear_type_code', '{{%arrear_type}}', 'code',
            'CASCADE',
            'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%tax_info}}');
        $this->dropTable('{{%taxpayer}}');
        $this->dropTable('{{%agency}}');
        $this->dropTable('{{%arrear_type}}');
    }
}
