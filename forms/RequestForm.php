<?php
/**
 *
 */

namespace app\forms;


use yii\base\Model;

/**
 * Class RequestForm
 *
 * @package app\forms
 */
class RequestForm extends Model
{
    /**
     * @var string
     */
    public $iin;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['iin', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'iin' => 'ИИН',
        ];
    }
}