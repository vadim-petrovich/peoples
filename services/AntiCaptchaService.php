<?php
/**
 *
 */

namespace app\services;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;
use yii\base\BaseObject;
use yii\helpers\FileHelper;

/**
 * Class AntiCaptchaService
 *
 * @package app\services
 */
class AntiCaptchaService extends BaseObject
{
    const RESOURCE_BASE_URI = 'http://kgd.gov.kz';
    const RESOURCE_SEARCH_URI = '/apps/services/culs-taxarrear-search-web/rest/search';
    const RESOURCE_CAPTCHA_URI = '/apps/services/CaptchaWeb/generate';

    /**
     * @var string
     */
    public $publicKey;
    /**
     * @var string
     */
    public $captchasDir = '@runtime/captchas';

    /**
     * @var string
     */
    private $_captchaUID;
    /**
     * @var string
     */
    private $_trickUID;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_captchaUID = $this->_generateUUID();
        $this->_trickUID   = $this->_generateUUID();

        FileHelper::createDirectory(\Yii::getAlias($this->captchasDir));
    }

    /**
     * @return string
     */
    private function _generateUUID()
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @param string $iin
     *
     * @return array
     * @throws AntiCaptchaException
     */
    public function pull(string $iin = null): array
    {
        $captchaResponse = $this->_requestCaptchaImage();
        $captchaFile     = $this->_saveCaptcha($captchaResponse);

        $api = new AntiCaptcha();
        $api->setKey($this->publicKey);
        $api->setVerboseMode(true);
        $api->setFile($captchaFile);

        if ( ! $api->createTask()) {
            throw new AntiCaptchaException("API v2 send failed - " . $api->getErrorMessage());
        }

        if ( ! $api->waitForResult()) {
            throw new AntiCaptchaException("could not solve captcha:" . $api->getErrorMessage());
        }

        $searchResponse = $this->_requestSearchData([
            'iinBin'             => $iin,
            'captcha-user-value' => $api->getTaskSolution(),
            'captcha-id'         => $this->_captchaUID,
        ]);

        $data = \GuzzleHttp\json_decode($searchResponse->getBody()->getContents(), true);
        if (empty($data) || ! isset($data['iinBin'])) {
            throw new AntiCaptchaException('Invalid data parsed');
        }

        return $data;
    }

    /**
     * @return ResponseInterface
     */
    private function _requestCaptchaImage(): ResponseInterface
    {
        return (new Client([
            'base_uri' => self::RESOURCE_BASE_URI,
        ]))->get(self::RESOURCE_CAPTCHA_URI, [
            'query' => [
                'uid' => $this->_captchaUID,
                't'   => $this->_trickUID,
            ],
        ]);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return string
     */
    private function _saveCaptcha(ResponseInterface $response): string
    {
        $file = \Yii::getAlias(sprintf(
            "%s/%s_%s.png",
            $this->captchasDir,
            $this->_captchaUID,
            $this->_trickUID
        ));

        return
            file_put_contents($file, $response->getBody()->getContents()) > 0
                ? $file
                : null;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface
     */
    private function _requestSearchData(array $data = []): ResponseInterface
    {
        return (new Client([
            'base_uri' => self::RESOURCE_BASE_URI,
        ]))->post(self::RESOURCE_SEARCH_URI, [
            'json' => $data,
        ]);
    }
}