<?php
/**
 *
 */

namespace app\services;

use app\models\Agency;
use app\models\ArrearType;
use app\models\TaxInfo;
use app\models\Taxpayer;
use yii\base\Exception;

/**
 * Сервис обработки данных по налогоплательщику
 *
 * @package app\services
 */
class DumperService
{

    /**
     * Обновление БД на основе всей информации полученной по налогоплательщику
     *
     * @param array $data
     *
     * @return bool
     * @throws DumperException
     */
    public function dump(array $data): bool
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $taxpayerIIN = $data['iinBin'];
            if ( ! $this->_taxpayerCreateIfNotExists(
                $taxpayerIIN,
                $data['name'],
                (float)$data['totalArrear'],
                (float)$data['pensionContributionArrear'],
                (float)$data['socialContributionArrear']
            )) {
                throw new Exception('Can not create taxpayer');
            }

            foreach ($data['taxOrgInfo'] as $agency) {
                $agencyCode = (int)$agency['charCode'];
                if ( ! $this->_agencyCreateIfNotExists(
                    $agencyCode,
                    $agency['nameRu'],
                    $agency['nameKk']
                )) {
                    throw new Exception('Can not create agency');
                }

                foreach ($agency['taxPayerInfo'] as $payerInfo) {
                    foreach ($payerInfo['bccArrearsInfo'] as $bccArrearsInfo) {
                        $arrearTypeCode = (int)$bccArrearsInfo['bcc'];
                        if ( ! $this->_arrearTypeCreateIfNotExists(
                            $arrearTypeCode,
                            $bccArrearsInfo['bccNameRu'],
                            $bccArrearsInfo['bccNameKz']
                        )) {
                            throw new Exception('Can not create arrear type');
                        }

                        if ( ! $this->_taxInfoCreateIfNotExists(
                            $taxpayerIIN,
                            $agencyCode,
                            $arrearTypeCode,
                            (float)$bccArrearsInfo['taxArrear'],
                            (float)$bccArrearsInfo['poenaArrear'],
                            (float)$bccArrearsInfo['fineArrear'],
                            (float)$bccArrearsInfo['totalArrear']
                        )) {
                            throw new Exception('Can not create tax info');
                        }
                    }
                }
            }
            $transaction->commit();

            return true;
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw new DumperException($exception->getMessage());
        }
    }


    /**
     * Создание нового налогоплательщика если он отсутствует в БД
     *
     * @param string $iin
     * @param string $name
     *
     * @param float  $totalArrear
     * @param float  $totalPensionContributionArrear
     * @param float  $totalSocialContributionArrear
     *
     * @return bool
     */
    private function _taxpayerCreateIfNotExists(
        string $iin,
        string $name,
        float $totalArrear,
        float $totalPensionContributionArrear,
        float $totalSocialContributionArrear
    ): bool {
        $taxpayer = Taxpayer::findOne(['iin' => $iin]);

        if ( ! $taxpayer) {
            $taxpayer = \Yii::createObject(Taxpayer::class);
        }
        $taxpayer->load([
            'iin'                               => $iin,
            'name'                              => $name,
            'total_arrear'                      => $totalArrear,
            'total_pension_contribution_arrear' => $totalPensionContributionArrear,
            'total_social_contribution_arrear'  => $totalSocialContributionArrear,
        ], '');

        return $taxpayer->save();
    }

    /**
     * Создание нового учреждения если оно отсутствует в БД
     *
     * @param int    $code
     * @param string $nameRu
     * @param string $nameKk
     *
     * @return bool
     */
    private function _agencyCreateIfNotExists(
        int $code,
        string $nameRu,
        string $nameKk
    ): bool {
        $agency = Agency::findOne(['code' => $code]);

        if ( ! $agency) {
            $agency = \Yii::createObject(Agency::class);
        }

        $agency->load([
            'code'   => $code,
            'nameRu' => $nameRu,
            'nameKz' => $nameKk,
        ], '');

        return $agency->save();
    }

    /**
     * Создание нового типа задолженности если оно отсутствует в БД
     *
     * @param int    $code
     * @param string $nameRu
     * @param string $nameKk
     *
     * @return bool
     */
    private function _arrearTypeCreateIfNotExists(
        int $code,
        string $nameRu,
        string $nameKk
    ): bool {
        $arrearType = ArrearType::findOne(['code' => $code]);

        if ( ! $arrearType) {
            $arrearType = \Yii::createObject(ArrearType::class);
        }

        $arrearType->load([
            'code'   => $code,
            'nameRu' => $nameRu,
            'nameKz' => $nameKk,
        ], '');

        return $arrearType->save();
    }

    /**
     * Создание/обновление информации по задолженности в разрезе учреждения и типа задолженности
     *
     * @param string $taxpayerIIN
     * @param int    $agencyCode
     * @param int    $arrearTypeCode
     * @param float  $bcc_tax_arrear
     * @param float  $bcc_poena_arrear
     * @param float  $bcc_fine_arrear
     * @param float  $bcc_total_arrear
     *
     * @return bool
     */
    private function _taxInfoCreateIfNotExists(
        string $taxpayerIIN,
        int $agencyCode,
        int $arrearTypeCode,
        float $bcc_tax_arrear,
        float $bcc_poena_arrear,
        float $bcc_fine_arrear,
        float $bcc_total_arrear
    ): bool {
        $taxInfo = TaxInfo::findOne([
            'taxpayer_iin'     => $taxpayerIIN,
            'agency_code'      => $agencyCode,
            'arrear_type_code' => $arrearTypeCode,
        ]);

        if ( ! $taxInfo) {
            $taxInfo = \Yii::createObject(TaxInfo::class);
        }

        $taxInfo->load([
            'taxpayer_iin'     => $taxpayerIIN,
            'agency_code'      => $agencyCode,
            'arrear_type_code' => $arrearTypeCode,
            'bcc_tax_arrear'   => $bcc_tax_arrear,
            'bcc_poena_arrear' => $bcc_poena_arrear,
            'bcc_fine_arrear'  => $bcc_fine_arrear,
            'bcc_total_arrear' => $bcc_total_arrear,
        ], '');

        return $taxInfo->save();
    }
}