<?php

return [
    'adminEmail' => 'admin@example.com',

    'antiCaptcha.publicKey' => 'ba774133fd19e03a02175d9f76fd9bc6',
    'antiCaptcha.dir'       => '@runtime/captchas',
];
