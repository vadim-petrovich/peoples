<?php

$config = [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset'  => 'utf8',
];

if (is_readable(__DIR__ . '/db.local.php')) {
    $config = array_merge($config, require __DIR__ . '/db.local.php');
}

return $config;