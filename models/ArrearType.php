<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%arrear_type}}".
 *
 * @property int $code Код КБК
 * @property string $nameRu Наименование КБК на русском
 * @property string $nameKz Наименование КБК на русском
 *
 * @property TaxInfo[] $taxInfos
 */
class ArrearType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%arrear_type}}';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('(%d) %s', $this->code, $this->nameRu);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'nameRu', 'nameKz'], 'required'],
            [['code'], 'default', 'value' => null],
            [['code'], 'integer'],
            [['nameRu', 'nameKz'], 'string'],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код КБК',
            'nameRu' => 'Наименование КБК на русском',
            'nameKz' => 'Наименование КБК на русском',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxInfos()
    {
        return $this->hasMany(TaxInfo::className(), ['arrear_type_code' => 'code']);
    }
}
