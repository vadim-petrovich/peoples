<?php

namespace app\models;

/**
 * This is the model class for table "{{%agency}}".
 *
 * @property int       $code   Код учреждения
 * @property string    $nameRu Наименование учреждения на русском
 * @property string    $nameKz Наименование учреждения на казахском
 *
 * @property TaxInfo[] $taxInfos
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'nameRu', 'nameKz'], 'required'],
            [['code'], 'default', 'value' => null],
            [['code'], 'integer'],
            [['nameRu', 'nameKz'], 'string'],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code'   => 'Код учреждения',
            'nameRu' => 'Наименование учреждения на русском',
            'nameKz' => 'Наименование учреждения на казахском',
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('(%d) %s', $this->code, $this->nameRu);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxInfos()
    {
        return $this->hasMany(TaxInfo::className(), ['agency_code' => 'code']);
    }
}
