<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tax_info}}".
 *
 * @property string $taxpayer_iin Налогоплательщик
 * @property int $agency_code Наименование учреждения
 * @property int $arrear_type_code Тип налоговой задолженности
 * @property string $bcc_tax_arrear Задолженность по сумме налога
 * @property string $bcc_poena_arrear Задолженность по сумме пени
 * @property string $bcc_fine_arrear Задолженность по сумме штрафа
 * @property string $bcc_total_arrear Всего задолженности
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Agency $agencyCode
 * @property ArrearType $arrearTypeCode
 * @property Taxpayer $taxpayerIin
 */
class TaxInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tax_info}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamable' => [
                'class' => TimestampBehavior::class,
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taxpayer_iin', 'agency_code', 'arrear_type_code'], 'required'],
            [['agency_code', 'arrear_type_code'], 'default', 'value' => null],
            [['agency_code', 'arrear_type_code'], 'integer'],
            [['bcc_tax_arrear', 'bcc_poena_arrear', 'bcc_fine_arrear', 'bcc_total_arrear'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['taxpayer_iin'], 'string', 'max' => 12],
            [['taxpayer_iin', 'agency_code', 'arrear_type_code'], 'unique', 'targetAttribute' => ['taxpayer_iin', 'agency_code', 'arrear_type_code']],
            [['agency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['agency_code' => 'code']],
            [['arrear_type_code'], 'exist', 'skipOnError' => true, 'targetClass' => ArrearType::className(), 'targetAttribute' => ['arrear_type_code' => 'code']],
            [['taxpayer_iin'], 'exist', 'skipOnError' => true, 'targetClass' => Taxpayer::className(), 'targetAttribute' => ['taxpayer_iin' => 'iin']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taxpayer_iin' => 'Налогоплательщик',
            'agency_code' => 'Наименование учреждения',
            'arrear_type_code' => 'Тип налоговой задолженности',
            'bcc_tax_arrear' => 'Задолженность по сумме налога',
            'bcc_poena_arrear' => 'Задолженность по сумме пени',
            'bcc_fine_arrear' => 'Задолженность по сумме штрафа',
            'bcc_total_arrear' => 'Всего задолженности',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'agencyCode' => 'Учреждение',
            'arrearTypeCode' => 'Тип задолженности',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencyCode()
    {
        return $this->hasOne(Agency::className(), ['code' => 'agency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrearTypeCode()
    {
        return $this->hasOne(ArrearType::className(), ['code' => 'arrear_type_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxpayerIin()
    {
        return $this->hasOne(Taxpayer::className(), ['iin' => 'taxpayer_iin']);
    }
}
