<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%taxpayer}}".
 *
 * @property string    $iin                               ИИН
 * @property string    $name                              Наименование налогоплательщика
 * @property string    $total_arrear                      Всего задолженности
 * @property string    $total_pension_contribution_arrear Задолженность по обязательным пенсионным взносам
 * @property string    $total_social_contribution_arrear  Задолженность по социальным отчислениям
 * @property string    $created_at                        Дата создания
 * @property string    $updated_at                        Дата обновления
 *
 * @property TaxInfo[] $taxInfos
 */
class Taxpayer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxpayer}}';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamable' => [
                'class' => TimestampBehavior::class,
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iin', 'name'], 'required'],
            [['total_arrear', 'total_pension_contribution_arrear', 'total_social_contribution_arrear'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['iin'], 'string', 'max' => 12],
            [['name'], 'string', 'max' => 255],
            [['iin'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iin'                               => 'ИИН',
            'name'                              => 'Наименование налогоплательщика',
            'total_arrear'                      => 'Всего задолженности',
            'total_pension_contribution_arrear' => 'Задолженность по обязательным пенсионным взносам',
            'total_social_contribution_arrear'  => 'Задолженность по социальным отчислениям',
            'created_at'                        => 'Дата создания',
            'updated_at'                        => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxInfos()
    {
        return $this->hasMany(TaxInfo::className(), ['taxpayer_iin' => 'iin'])->joinWith([
            'agencyCode',
            'arrearTypeCode',
        ]);
    }
}
