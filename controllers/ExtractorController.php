<?php
/**
 *
 */

namespace app\controllers;


use app\forms\RequestForm;
use app\models\Taxpayer;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ExtractorController
 *
 * @package app\controllers
 */
class ExtractorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'search' => ['post'],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $taxpayerDataProvider = new ActiveDataProvider([
            'query' => Taxpayer::find(),
        ]);

        return $this->render('index', [
            'taxpayerDataProvider' => $taxpayerDataProvider,
            'requestForm'          => \Yii::createObject(RequestForm::class),
        ]);
    }

    /**
     * @param string $id IIN
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $iin = $id;

        $taxpayer = Taxpayer::findOne(['iin' => $iin]);
        if ( ! $taxpayer) {
            throw new NotFoundHttpException('Taxpayer not found');
        }

        return $this->render('view', [
            'taxpayer' => $taxpayer,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionSearch()
    {
        $requestForm = \Yii::createObject(RequestForm::class);
        $requestForm->load(\Yii::$app->request->post());

        $data = \Yii::$app->extractor->pull($requestForm->iin);

        \Yii::$app->extractor->dump($data)
            ? \Yii::$app->session->addFlash('success', 'Success')
            : \Yii::$app->session->addFlash('error', 'Something went wrong');

        return $this->redirect('index');
    }
}